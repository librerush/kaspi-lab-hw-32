FROM gradle:jdk11 as builder
COPY --chown=gradle:gradle . .
RUN gradle build

FROM openjdk:11.0.10-jre-slim-buster
RUN addgroup appuser && adduser --disabled-password --ingroup appuser appuser
WORKDIR /app
RUN chown appuser:appuser /app
USER appuser
COPY --from=builder --chown=appuser:appuser /home/gradle/build/libs/hw32-0.0.1.jar /app/main.jar
EXPOSE 8080
ENTRYPOINT exec java -jar main.jar --spring.profiles.active=\${ACTIVE_PROFILE}