package kz.kaspi.hw32.service;

import kz.kaspi.hw32.dto.Day;
import kz.kaspi.hw32.dto.PeriodInfoDTO;
import kz.kaspi.hw32.dto.PeriodInfoRequest;
import kz.kaspi.hw32.entity.HolidayEntity;
import kz.kaspi.hw32.entity.WorkdayEntity;
import kz.kaspi.hw32.repository.HolidayRepository;
import kz.kaspi.hw32.repository.WorkdayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class MainService {
    private final HolidayRepository holidayRepository;

    private final WorkdayRepository workdayRepository;

    @Autowired
    public MainService(HolidayRepository holidayRepository, WorkdayRepository workdayRepository) {
        this.holidayRepository = holidayRepository;
        this.workdayRepository = workdayRepository;
    }

    public PeriodInfoDTO holidaysInPeriod(PeriodInfoRequest request) {
        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);

        long a = DAYS.between(dateFrom, dateTo);

        long b = getWeekends(dateFrom, dateTo);
        long c = getHolidays(dateFrom, dateTo);
        long d = getWorkdays(dateFrom, dateTo);

        long holidays = b + c - d;
        long workdays = a - holidays;

        return new PeriodInfoDTO(workdays, holidays);
    }

    private long getWorkdays(LocalDate dateFrom, LocalDate dateTo) {
        return workdayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getHolidays(LocalDate dateFrom, LocalDate dateTo) {
        return holidayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getWeekends(LocalDate dateFrom, LocalDate dateTo) {
        return dateFrom.datesUntil(dateTo)
                .filter(this::isWeekend)
                .count();
    }

    public List<Day> listInPeriod(PeriodInfoRequest request) {

        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);

        List<WorkdayEntity> workdayEntityList = workdayRepository.findAll();
        List<HolidayEntity> holidayEntityList = holidayRepository.findAll();

        return dateFrom.datesUntil(dateTo)
                .map(localDate -> {
                    boolean isHoliday = (
                            isWeekend(localDate)
                                    && isNotWorkingDay(localDate, workdayEntityList)
                    ) ||
                            isHoliday(localDate, holidayEntityList);
                    return new Day(localDate, isHoliday);
                }).collect(Collectors.toList());
    }

    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek() == DayOfWeek.SATURDAY ||
                date.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    private boolean isHoliday(LocalDate date, List<HolidayEntity> holidayEntityList) {
        return holidayEntityList.stream()
                .anyMatch(it -> it.getDate().isEqual(date));
    }

    private boolean isNotWorkingDay(LocalDate date, List<WorkdayEntity> workdayEntityList) {
        return workdayEntityList.stream()
                .noneMatch(it -> it.getDate().isEqual(date));
    }
}
