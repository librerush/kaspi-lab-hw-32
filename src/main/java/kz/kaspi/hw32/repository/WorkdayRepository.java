package kz.kaspi.hw32.repository;

import kz.kaspi.hw32.entity.WorkdayEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface WorkdayRepository extends JpaRepository<WorkdayEntity, Long> {
    List<WorkdayEntity> findAllByDateGreaterThanEqualAndDateLessThanEqual(LocalDate dateFrom, LocalDate dateTo);
}
